/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package in.co.triomoney.model;

import java.io.Serializable;
import java.util.List;


public class Fee implements Serializable {
    private static final long serialVersionUID = 1L;
    
    private Long id;
    
    private long transTypeID;
    
    private long channelID;
    
    private int productID;
    
    private long circleID;
    
    private long groupID;
    
    private long transAccountTypeID;
    
    private long targetAccountTypeID;
    
    private String method;
    
    private String fromAmount;
    
    private String toAmount;
    
    private String feeAmount;
   
    private String feePercentage;
    
    private String feeSource;
    
    private String feeTarget;
    private List<Fee> feeList;
    public Fee() {
    }

    public Fee(Long id) {
        this.id = id;
    }

    public Fee(Long id, long transTypeID, long channelID, long circleID, long groupID, long transAccountTypeID, long targetAccountTypeID, String method, String fromAmount, String toAmount, String feeAmount, String feePercentage, String feeSource, String feeTarget) {
        this.id = id;
        this.transTypeID = transTypeID;
        this.channelID = channelID;
        this.circleID = circleID;
        this.groupID = groupID;
        this.transAccountTypeID = transAccountTypeID;
        this.targetAccountTypeID = targetAccountTypeID;
        this.method = method;
        this.fromAmount = fromAmount;
        this.toAmount = toAmount;
        this.feeAmount = feeAmount;
        this.feePercentage = feePercentage;
        this.feeSource = feeSource;
        this.feeTarget = feeTarget;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public long getTransTypeID() {
        return transTypeID;
    }

    public void setTransTypeID(long transTypeID) {
        this.transTypeID = transTypeID;
    }

    public long getChannelID() {
        return channelID;
    }

    public void setChannelID(long channelID) {
        this.channelID = channelID;
    }

    public long getCircleID() {
        return circleID;
    }

    public void setCircleID(long circleID) {
        this.circleID = circleID;
    }

    public long getGroupID() {
        return groupID;
    }

    public void setGroupID(long groupID) {
        this.groupID = groupID;
    }

    public long getTransAccountTypeID() {
        return transAccountTypeID;
    }

    public void setTransAccountTypeID(long transAccountTypeID) {
        this.transAccountTypeID = transAccountTypeID;
    }

    public long getTargetAccountTypeID() {
        return targetAccountTypeID;
    }

    public void setTargetAccountTypeID(long targetAccountTypeID) {
        this.targetAccountTypeID = targetAccountTypeID;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getFromAmount() {
        return fromAmount;
    }

    public void setFromAmount(String fromAmount) {
        this.fromAmount = fromAmount;
    }

    public String getToAmount() {
        return toAmount;
    }

    public void setToAmount(String toAmount) {
        this.toAmount = toAmount;
    }

    public String getFeeAmount() {
        return feeAmount;
    }

    public void setFeeAmount(String feeAmount) {
        this.feeAmount = feeAmount;
    }

    public String getFeePercentage() {
        return feePercentage;
    }

    public void setFeePercentage(String feePercentage) {
        this.feePercentage = feePercentage;
    }
    public String getFeeSource() {
        return feeSource;
    }

    public void setFeeSource(String feeSource) {
        this.feeSource = feeSource;
    }

    public String getFeeTarget() {
        return feeTarget;
    }

    public void setFeeTarget(String feeTarget) {
        this.feeTarget = feeTarget;
    }
     
    public List<Fee> getFeeList() {
		return feeList;
	}

	public void setFeeList(List<Fee> feeList) {
		this.feeList = feeList;
	}


	public int getProductID() {
		return productID;
	}

	public void setProductID(int productID) {
		this.productID = productID;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Fee)) {
            return false;
        }
        Fee other = (Fee) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "in.co.triotech.model.Fee[ id=" + id + " ]";
    }
    
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package in.co.triomoney.model;

import java.io.Serializable;

public class EntityGroupMapping implements Serializable {
    private static final long serialVersionUID = 1L;
   
    private Long id;
    
    private long entityID;
    
    private long groupID;

    public EntityGroupMapping() {
    }

    public EntityGroupMapping(Long id) {
        this.id = id;
    }

    public EntityGroupMapping(Long id, long entityID, long groupID) {
        this.id = id;
        this.entityID = entityID;
        this.groupID = groupID;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public long getEntityID() {
        return entityID;
    }

    public void setEntityID(long entityID) {
        this.entityID = entityID;
    }

    public long getGroupID() {
        return groupID;
    }

    public void setGroupID(long groupID) {
        this.groupID = groupID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EntityGroupMapping)) {
            return false;
        }
        EntityGroupMapping other = (EntityGroupMapping) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "in.co.triotech.model.EntityGroupMapping[ id=" + id + " ]";
    }
    
}

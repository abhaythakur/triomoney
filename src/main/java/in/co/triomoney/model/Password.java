/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package in.co.triomoney.model;

import java.io.Serializable;
import java.util.Date;

public class Password implements Serializable {
    private static final long serialVersionUID = 1L;
    
    private Long id;
    
    private long entityID;
    
    private String password;
    
    private Date lastModified;
    
    private String status;
    
    private String incorrectAttempts;
    
    private String old1;
    
    private String old2;
    
    private String old3;
    
    private String old4;
    
    private String old5;

    public Password() {
    }

    public Password(Long id) {
        this.id = id;
    }

    public Password(Long id, long entityID, String password, Date lastModified, String status,String incorrectAttempts,String old1,String old2,String old3,String old4,String old5) {
        this.id = id;
        this.entityID = entityID;
        this.password = password;
        this.lastModified = lastModified;
        this.incorrectAttempts = incorrectAttempts;
        this.old1 = old1;
        this.old2 = old2;
        this.old3 = old3;
        this.old4 = old4;
        this.old5 = old5;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public long getEntityID() {
        return entityID;
    }

    public void setEntityID(long entityID) {
        this.entityID = entityID;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getLastModified() {
        return lastModified;
    }

    public void setLastModified(Date lastModified) {
        this.lastModified = lastModified;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getIncorrectAttempts() {
        return incorrectAttempts;
    }

    public void setIncorrectAttempts(String incorrectAttempts) {
        this.incorrectAttempts = incorrectAttempts;
    }

    public String getOld1() {
        return old1;
    }

    public void setOld1(String old1) {
        this.old1 = old1;
    }

    public String getOld2() {
        return old2;
    }

    public void setOld2(String old2) {
        this.old2 = old2;
    }

    public String getOld3() {
        return old3;
    }

    public void setOld3(String old3) {
        this.old3 = old3;
    }

    public String getOld4() {
        return old4;
    }

    public void setOld4(String old4) {
        this.old4 = old4;
    }

    public String getOld5() {
        return old5;
    }

    public void setOld5(String old5) {
        this.old5 = old5;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Password)) {
            return false;
        }
        Password other = (Password) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "in.co.triotech.model.Password[ id=" + id + " ]";
    }
    
}

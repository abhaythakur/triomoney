package in.co.triomoney.model;

import java.util.Date;

public class TransExtDetails {

	
private static final long serialVersionUID = 1L;
    
    private Long id;
    
    private Long transID;
        
    private Date created;
    
    private Date lastModified;
    
    private String request;
    
    private String response;
    
    private String extResult;
    
    private String extResultDescription;

    public TransExtDetails(){
    	
    }
    
    
	public TransExtDetails(Long id, Long transID, Date created, Date lastModified, String request, String response,
			String extResult, String extResultDescription) {
		super();
		this.id = id;
		this.transID = transID;
		this.created = created;
		this.lastModified = lastModified;
		this.request = request;
		this.response = response;
		this.extResult = extResult;
		this.extResultDescription = extResultDescription;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getTransID() {
		return transID;
	}

	public void setTransID(Long transID) {
		this.transID = transID;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getLastModified() {
		return lastModified;
	}

	public void setLastModified(Date lastModified) {
		this.lastModified = lastModified;
	}

	public String getRequest() {
		return request;
	}

	public void setRequest(String request) {
		this.request = request;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public String getExtResult() {
		return extResult;
	}

	public void setExtResult(String extResult) {
		this.extResult = extResult;
	}

	public String getExtResultDescription() {
		return extResultDescription;
	}

	public void setExtResultDescription(String extResultDescription) {
		this.extResultDescription = extResultDescription;
	}

    
    
}

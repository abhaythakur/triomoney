/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package in.co.triomoney.model;

import java.io.Serializable;
import java.util.Date;


public class Transaction implements Serializable {
    public Transaction(String initiator) {
		super();
		this.initiator = initiator;
	}

	private static final long serialVersionUID = 1L;
    
    private Long id;
    
    private String initiator;
    
    private long transTypeID;
    
    private Date created;
    
    private Date lastModified;
    
    private String debtor;
    
    private String creditor;
    
    private String recipient;
    
    private String amount;
    
    private String productID;
        
    private String state;
    
    private long resultID;
    
    private long parentID;
    
    private long channelID;
    
    private String param1;
    
    private String param2;
   
    private String param3;
    
    private Boolean isAtomic;
    
    private String resultDesc;
    public Transaction(Transaction t) {
		super();
		this.id = t.id;
		this.initiator = t.initiator;
		this.transTypeID = t.transTypeID;
		this.created = t.created;
		this.lastModified = t.lastModified;
		this.debtor = t.debtor;
		this.creditor = t.creditor;
		this.amount = t.amount;
		this.state = t.state;
		this.resultID = t.resultID;
		this.parentID = t.parentID;
		this.channelID = t.channelID;
		this.param1 = t.param1;
		this.param2 = t.param2;
		this.param3 = t.param3;
		this.recipient = t.recipient;
		this.productID = t.productID;
		this.isAtomic = t.isAtomic;
	}

    public Transaction(Long id, String initiator, long transTypeID, Date created,
			Date lastModified, String debtor, String creditor, String amount,
			String state, long resultID, long parentID, long channelID,
			String param1, String param2, String param3,String recipient,String productID, Boolean isAtomic) {
		super();
		this.id = id;
		this.initiator = initiator;
		this.transTypeID = transTypeID;
		this.created = created;
		this.lastModified = lastModified;
		this.debtor = debtor;
		this.creditor = creditor;
		this.amount = amount;
		this.state = state;
		this.resultID = resultID;
		this.parentID = parentID;
		this.channelID = channelID;
		this.param1 = param1;
		this.param2 = param2;
		this.param3 = param3;
		this.recipient = recipient;
		this.productID = productID;
		this.isAtomic = isAtomic;
	}
    public Transaction(Long id, String initiator, long transTypeID, Date created,
			Date lastModified, String debtor, String creditor, String amount,
			String state, long resultID, long parentID, long channelID,
			String param1, String param2, String param3,String recipient,
			String productID, Boolean isAtomic,String resultDesc) {
		super();
		this.id = id;
		this.initiator = initiator;
		this.transTypeID = transTypeID;
		this.created = created;
		this.lastModified = lastModified;
		this.debtor = debtor;
		this.creditor = creditor;
		this.amount = amount;
		this.state = state;
		this.resultID = resultID;
		this.parentID = parentID;
		this.channelID = channelID;
		this.param1 = param1;
		this.param2 = param2;
		this.param3 = param3;
		this.recipient = recipient;
		this.productID = productID;
		this.isAtomic = isAtomic;
		this.resultDesc =resultDesc;
	}
	public Transaction() {
    }

    public Transaction(String initiator, long transTypeID, Date created,
			Date lastModified, String debtor, String creditor, String amount,
			String state, long resultID, long parentID, long channelID,String param3,
			Boolean isAtomic) {
		super();
		this.initiator = initiator;
		this.transTypeID = transTypeID;
		this.created = created;
		this.lastModified = lastModified;
		this.debtor = debtor;
		this.creditor = creditor;
		this.amount = amount;
		this.state = state;
		this.resultID = resultID;
		this.parentID = parentID;
		this.channelID = channelID;
		this.param3=param3;
		this.isAtomic = isAtomic;
	}
    public Transaction(String initiator, long transTypeID,String productID, Date created,
			Date lastModified, String debtor, String creditor, String amount,
			String state, long resultID, long parentID, long channelID,
			Boolean isAtomic) {
		super();
		this.initiator = initiator;
		this.transTypeID = transTypeID;
		this.productID = productID;
		this.created = created;
		this.lastModified = lastModified;
		this.debtor = debtor;
		this.creditor = creditor;
		this.amount = amount;
		this.state = state;
		this.resultID = resultID;
		this.parentID = parentID;
		this.channelID = channelID;
		this.isAtomic = isAtomic;
	}

	public Transaction(Long id) {
        this.id = id;
    }
	 public Transaction(String initiator, long transTypeID, Date created,
				Date lastModified, String debtor, String creditor, String amount,
				String state, long resultID, long parentID, long channelID,
				Boolean isAtomic) {
			super();
			this.initiator = initiator;
			this.transTypeID = transTypeID;
			this.created = created;
			this.lastModified = lastModified;
			this.debtor = debtor;
			this.creditor = creditor;
			this.amount = amount;
			this.state = state;
			this.resultID = resultID;
			this.parentID = parentID;
			this.channelID = channelID;
			this.isAtomic = isAtomic;
		}
    public Transaction(Long id, String initiator, long transTypeID, Date created, String debtor, String creditor, String state, long parentID, long channelID, Boolean isAtomic) {
        this.id = id;
        this.initiator = initiator;
        this.transTypeID = transTypeID;
        this.created = created;
        this.debtor = debtor;
        this.creditor = creditor;
        this.state = state;
        this.parentID = parentID;
        this.channelID = channelID;
        this.isAtomic = isAtomic;
    }
    public Transaction( String initiator, long transTypeID, Date created, String debtor, String creditor, String state, long parentID, long channelID, Boolean isAtomic) {
        
        this.initiator = initiator;
        this.transTypeID = transTypeID;
        this.created = created;
        this.debtor = debtor;
        this.creditor = creditor;
        this.state = state;
        this.parentID = parentID;
        this.channelID = channelID;
        this.isAtomic = isAtomic;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getInitiator() {
        return initiator;
    }

    public void setInitiator(String initiator) {
        this.initiator = initiator;
    }

    public long getTransTypeID() {
        return transTypeID;
    }

    public void setTransTypeID(long transTypeID) {
        this.transTypeID = transTypeID;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getLastModified() {
        return lastModified;
    }

    public void setLastModified(Date lastModified) {
        this.lastModified = lastModified;
    }

    public String getRecipient() {
		return recipient;
	}

	public void setRecipient(String recipient) {
		this.recipient = recipient;
	}

	public String getProductID() {
		return productID;
	}

	public void setProductID(String productID) {
		this.productID = productID;
	}

	public String getDebtor() {
        return debtor;
    }

    public void setDebtor(String debtor) {
        this.debtor = debtor;
    }

    public String getCreditor() {
        return creditor;
    }

    public void setCreditor(String creditor) {
        this.creditor = creditor;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public long getResultID() {
        return resultID;
    }

    public void setResultID(long resultID) {
        this.resultID = resultID;
    }

    public long getParentID() {
        return parentID;
    }

    public void setParentID(long parentID) {
        this.parentID = parentID;
    }

    public long getChannelID() {
        return channelID;
    }

    public void setChannelID(long channelID) {
        this.channelID = channelID;
    }

    public String getParam1() {
        return param1;
    }

    public void setParam1(String param1) {
        this.param1 = param1;
    }

    public String getParam2() {
        return param2;
    }

    public void setParam2(String param2) {
        this.param2 = param2;
    }

    public String getParam3() {
        return param3;
    }

    public void setParam3(String param3) {
        this.param3 = param3;
    }

    public Boolean getIsAtomic() {
		return isAtomic;
	}

	public void setIsAtomic(Boolean isAtomic) {
		this.isAtomic = isAtomic;
	}
	
	public String getResultDesc() {
		return resultDesc;
	}

	public void setResultDesc(String resultDesc) {
		this.resultDesc = resultDesc;
	}

	@Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Transaction)) {
            return false;
        }
        Transaction other = (Transaction) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

	@Override
	public String toString() {
		return "Trans [id=" + id + ", initiator=" + initiator
				+ ", transTypeID=" + transTypeID + ", created=" + created
				+ ", lastModified=" + lastModified + ", debtor=" + debtor
				+ ", creditor=" + creditor + ", amount=" + amount + ", state="
				+ state + ", resultID=" + resultID + ", parentID=" + parentID
				+ ", channelID=" + channelID + ", param1=" + param1
				+ ", param2=" + param2 + ", param3=" + param3 + ", isAtomic="
				+ isAtomic + "]";
	}

   
    
    
}

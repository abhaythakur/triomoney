/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package in.co.triomoney.model;

import java.io.Serializable;
import java.text.DecimalFormat;

public class Accounts implements Serializable {
    private static final long serialVersionUID = 1L;
    private  DecimalFormat df2 = new DecimalFormat(".##");
    private Long id;
   
    private long entityID;
    
    private double currentBalance;
    
    private double availableBalance;
    
    private String lowerLimit;
    
    private String upperLimit;
   
    private Long typeID;

    public Accounts() {
    }

    public Accounts(Long id) {
        this.id = id;
    }

    public Accounts(Long id, long entityID, double currentBalance, double availableBalance, String lowerLimit, String upperLimit, Long typeID) {
        this.id = id;
        this.entityID = entityID;
        this.currentBalance = currentBalance;
        this.availableBalance = availableBalance;
        this.lowerLimit = lowerLimit;
        this.upperLimit = upperLimit;        this.typeID = typeID;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public long getEntityID() {
        return entityID;
    }

    public void setEntityID(long entityID) {
        this.entityID = entityID;
    }

    public double getCurrentBalance() {
		return Double.parseDouble(df2.format(currentBalance));
	}

	public void setCurrentBalance(double currentBalance) {
		this.currentBalance = currentBalance;
	}

	public double getAvailableBalance() {
		return Double.parseDouble(df2.format(availableBalance));
	}

	public void setAvailableBalance(double availableBalance) {
		this.availableBalance = availableBalance;
	}

	public String getLowerLimit() {
        return lowerLimit;
    }

    public void setLowerLimit(String lowerLimit) {
        this.lowerLimit = lowerLimit;
    }

    public String getUpperLimit() {
        return upperLimit;
    }

    public void setUpperLimit(String upperLimit) {
        this.upperLimit = upperLimit;
    }

    public Long getTypeID() {
        return typeID;
    }

    public void setTypeID(Long typeID) {
        this.typeID = typeID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Accounts)) {
            return false;
        }
        Accounts other = (Accounts) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

	@Override
	public String toString() {
		return "Accounts [id=" + id + ", entityID=" + entityID + ", currentBalance=" + currentBalance
				+ ", availableBalance=" + availableBalance + ", lowerLimit=" + lowerLimit + ", upperLimit=" + upperLimit
				+ ", typeID=" + typeID + "]";
	}



   
    
}

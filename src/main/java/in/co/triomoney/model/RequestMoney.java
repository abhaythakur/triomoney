package in.co.triomoney.model;

public class RequestMoney {

	 private static final long serialVersionUID = 1L;
	    
	    private Long id;
	   
	    private long entityID;
	    
	    private String transID;
	    	    
	    private String event;
	    
	    private String name;
	    
	    private String mobile;
	    
	    private String amount;
	   
	    private String created;

	    public RequestMoney() {
	    	
	    }

		public Long getId() {
			return id;
		}

		public long getEntityID() {
			return entityID;
		}

		public String getTransID() {
			return transID;
		}

		public String getEvent() {
			return event;
		}

		public String getName() {
			return name;
		}

		public String getMobile() {
			return mobile;
		}

		public String getAmount() {
			return amount;
		}

		public String getCreated() {
			return created;
		}

		public void setId(Long id) {
			this.id = id;
		}

		public void setEntityID(long entityID) {
			this.entityID = entityID;
		}

		public void setTransID(String transID) {
			this.transID = transID;
		}

		public void setEvent(String event) {
			this.event = event;
		}

		public void setName(String name) {
			this.name = name;
		}

		public void setMobile(String mobile) {
			this.mobile = mobile;
		}

		public void setAmount(String amount) {
			this.amount = amount;
		}

		public void setCreated(String created) {
			this.created = created;
		}

		public RequestMoney(String transID, String event, String name, String mobile, String amount, String created) {
			this.transID = transID;
			this.event = event;
			this.name = name;
			this.mobile = mobile;
			this.amount = amount;
			this.created = created;
		}

		@Override
		public String toString() {
			return "RequestMoney [transID=" + transID + ", event=" + event + ", name=" + name + ", amount=" + amount
					+ ", created=" + created + "]";
		}

	
		

	
		
	    
}

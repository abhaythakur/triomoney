package in.co.triomoney.model;

public class Transdetail {
	private Object tid;
	private Object mpin;
	private Transdetail transdetail;
	private String type;
	private String status;

	public Transdetail() {

	}

	public Object getTid() {
		return tid;
	}

	public void setTid(Object tid) {
		this.tid = tid;
	}

	public Object getMpin() {
		return mpin;
	}

	public void setMpin(Object mpin) {
		this.mpin = mpin;
	}

	public Transdetail getTransdetail() {
		return transdetail;
	}

	public void setTransdetail(Transdetail transdetail) {
		this.transdetail = transdetail;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package in.co.triomoney.model;

import java.io.Serializable;
import java.util.Date;


public class Voucher implements Serializable {
    private static final long serialVersionUID = 1L;
    
    private Long id;
    
    private long transID;
    
    private Date created;
    
    private Date lastModified;
    
    private String idType;
    
    private String idNumber;
    
    private long entityID;
    
    private String status;
   
    private String fromMSISDN;
    
    private String toMSISDN;
    
    private String fromName;
    
    private String toName;
    
    private String amount;
    
    private String targetPIN;
    
    private String cashoutTID;
    
    private String param1;
    
    private String param2;
    
    private String param3;

    public Voucher() {
    }

    public Voucher(Long id) {
        this.id = id;
    }

    public Voucher(Long id, long transID, Date created, Date lastModified,String idType,String idNumber, long entityID, String status, String fromMSISDN, String toMSISDN, String fromName, String toName, String amount, String targetPIN, String cashoutTID, String param1, String param2, String param3) {
        this.id = id;
        this.transID = transID;
        this.created = created;
        this.lastModified = lastModified;
        this.idType=idType;
        this.idNumber=idNumber;
       
        this.entityID = entityID;
        this.status = status;
        this.fromMSISDN = fromMSISDN;
        this.toMSISDN = toMSISDN;
        this.fromName = fromName;
        this.toName = toName;
        this.amount = amount;
        this.targetPIN = targetPIN;
        this.cashoutTID = cashoutTID;
        this.param1 = param1;
        this.param2 = param2;
        this.param3 = param3;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public long getTransID() {
        return transID;
    }

    public void setTransID(long transID) {
        this.transID = transID;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getLastModified() {
        return lastModified;
    }
    
    public String getIdType() {
		return idType;
	}

	public void setIdType(String idType) {
		this.idType = idType;
	}

	public String getIdNumber() {
		return idNumber;
	}

	public void setIdNumber(String idNumber) {
		this.idNumber = idNumber;
	}

	public void setLastModified(Date lastModified) {
        this.lastModified = lastModified;
    }

    public long getEntityID() {
        return entityID;
    }

    public void setEntityID(long entityID) {
        this.entityID = entityID;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getFromMSISDN() {
        return fromMSISDN;
    }

    public void setFromMSISDN(String fromMSISDN) {
        this.fromMSISDN = fromMSISDN;
    }

    public String getToMSISDN() {
        return toMSISDN;
    }

    public void setToMSISDN(String toMSISDN) {
        this.toMSISDN = toMSISDN;
    }

    public String getFromName() {
        return fromName;
    }

    public void setFromName(String fromName) {
        this.fromName = fromName;
    }

    public String getToName() {
        return toName;
    }

    public void setToName(String toName) {
        this.toName = toName;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getTargetPIN() {
        return targetPIN;
    }

    public void setTargetPIN(String targetPIN) {
        this.targetPIN = targetPIN;
    }

    public String getCashoutTID() {
        return cashoutTID;
    }

    public void setCashoutTID(String cashoutTID) {
        this.cashoutTID = cashoutTID;
    }

    public String getParam1() {
        return param1;
    }

    public void setParam1(String param1) {
        this.param1 = param1;
    }

    public String getParam2() {
        return param2;
    }

    public void setParam2(String param2) {
        this.param2 = param2;
    }

    public String getParam3() {
        return param3;
    }

    public void setParam3(String param3) {
        this.param3 = param3;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Voucher)) {
            return false;
        }
        Voucher other = (Voucher) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "in.co.triotech.model.Voucher[ id=" + id + " ]";
    }
    
}

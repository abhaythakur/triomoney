/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package in.co.triomoney.model;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class SystemConfig implements RowMapper<SystemConfig> {

	private Long id;

	private String sys_key;

	private String sys_val;

	public SystemConfig() {
	}

	public SystemConfig(Long id) {
		this.id = id;
	}

	public SystemConfig(Long id, String sys_key, String sys_val) {
		this.id = id;
		this.sys_key = sys_key;
		this.sys_val = sys_val;
	}

	public String getSys_key() {
		return sys_key;
	}

	public void setSys_key(String sys_key) {
		this.sys_key = sys_key;
	}

	public String getSys_val() {
		return sys_val;
	}

	public void setSys_val(String sys_val) {
		this.sys_val = sys_val;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public SystemConfig mapRow(ResultSet row, int rowNum) throws SQLException {
		SystemConfig s = new SystemConfig();
		s.setId(row.getLong("id"));
		s.setSys_key(row.getString("sys_key"));
		s.setSys_val(row.getString("sys_val"));
		return s;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof SystemConfig)) {
			return false;
		}
		SystemConfig other = (SystemConfig) object;
		if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "in.co.triotech.model.SystemConfig[ id=" + id + " ]";
	}

}

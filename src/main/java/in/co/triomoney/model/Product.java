package in.co.triomoney.model;

public class Product {
	
	    private Integer id;
	    private String name;
	    private Boolean isActive;
	    private Integer productTypeID;
	    private String command;

	    public Product() {
	    }

	    public Product(Integer id) {
	        this.id = id;
	    }

	    public Integer getId() {
	        return id;
	    }

	    public void setId(Integer id) {
	        this.id = id;
	    }

	    public String getName() {
	        return name;
	    }

	    public void setName(String name) {
	        this.name = name;
	    }

	 
	    public Boolean getIsActive() {
			return isActive;
		}

		public void setIsActive(Boolean isActive) {
			this.isActive = isActive;
		}

		public Integer getProductTypeID() {
	        return productTypeID;
	    }

	    public void setProductTypeID(Integer productTypeID) {
	        this.productTypeID = productTypeID;
	    }

	    public String getCommand() {
	        return command;
	    }

	    public void setCommand(String command) {
	        this.command = command;
	    }


}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package in.co.triomoney.model;

import java.io.Serializable;


public class Entity implements Serializable {
    private static final long serialVersionUID = 1L;
    
    private Long id;
   
    private String msisdn;
   
    private String name;
   
    private long statusID;
    
    private long typeID;
    
    private long parentID;
    
    private long circleID;
    
    private String pin;
    
    private String param1;
    
    private String param2;
    
    private String param3;
    
    private String last_login;
    
    private String franchise;

    public Entity() {
    }

    public Entity(Long id) {
        this.id = id;
    }

    public Entity(Long id, String msisdn, String name, long statusID, long typeID, long parentID, long circleID, String pin,String param1,String param2,String param3,String last_login) {
        this.id = id;
        this.msisdn = msisdn;
        this.name = name;
        this.statusID = statusID;
        this.typeID = typeID;
        this.parentID = parentID;
        this.circleID = circleID;
        this.param1 = param1;
        this.param2 = param2;
        this.param3 = param3;
        this.pin = pin;
        this.last_login=last_login;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getStatusID() {
        return statusID;
    }

    public void setStatusID(long statusID) {
        this.statusID = statusID;
    }

    public long getTypeID() {
        return typeID;
    }

    public void setTypeID(long typeID) {
        this.typeID = typeID;
    }

    public long getParentID() {
        return parentID;
    }

    public void setParentID(long parentID) {
        this.parentID = parentID;
    }

    public long getCircleID() {
        return circleID;
    }

    public void setCircleID(long circleID) {
        this.circleID = circleID;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public String getParam1() {
        return param1;
    }

    public void setParam1(String param1) {
        this.param1 = param1;
    }

    public String getParam2() {
        return param2;
    }

    public String getFranchise() {
		return franchise;
	}

	public void setFranchise(String franchise) {
		this.franchise = franchise;
	}

	public void setParam2(String param2) {
        this.param2 = param2;
    }

    public String getParam3() {
        return param3;
    }

    public void setParam3(String param3) {
        this.param3 = param3;
    }

    public String getLast_login() {
		return last_login;
	}

	public void setLast_login(String last_login) {
		this.last_login = last_login;
	}

	@Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }
	
    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Entity)) {
            return false;
        }
        Entity other = (Entity) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "in.co.triotech.model.Entity[ id=" + id + " ]";
    }
    
}

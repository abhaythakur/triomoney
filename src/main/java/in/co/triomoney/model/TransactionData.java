package in.co.triomoney.model;

public class TransactionData {

	private long transID;
	private String keyType;
	private String keyVal;
	
	public TransactionData() {
	}
	public long getTransID() {
		return transID;
	}
	public void setTransID(long transID) {
		this.transID = transID;
	}
	public String getKeyType() {
		return keyType;
	}
	public void setKeyType(String keyType) {
		this.keyType = keyType;
	}
	public String getKeyVal() {
		return keyVal;
	}
	public void setKeyVal(String keyVal) {
		this.keyVal = keyVal;
	}
	public TransactionData(long transID, String keyType, String keyVal) {
		this.transID = transID;
		this.keyType = keyType;
		this.keyVal = keyVal;
	}
	
	
}

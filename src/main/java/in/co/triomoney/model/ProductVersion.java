package in.co.triomoney.model;

public class ProductVersion {
	    private Integer id;
	    private String version;
	    private Integer isActive;
	    
	    public ProductVersion(){
	    	
	    }
		public Integer getId() {
			return id;
		}
		public void setId(Integer id) {
			this.id = id;
		}
		public String getVersion() {
			return version;
		}
		public void setVersion(String version) {
			this.version = version;
		}
		public Integer getIsActive() {
			return isActive;
		}
		public void setIsActive(Integer isActive) {
			this.isActive = isActive;
		}
	    
	    
}

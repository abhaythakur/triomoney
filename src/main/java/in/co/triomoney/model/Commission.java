/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package in.co.triomoney.model;

import java.io.Serializable;
import java.util.List;

public class Commission implements Serializable {
    private static final long serialVersionUID = 1L;
   
    private Long id;
    
    private long transTypeID;
   
    private long channelID;
    
    private int productID;
    
    private long circleID;
    
    private long groupID;
    
    private long transAccountTypeID;
    
    private long targetAccountTypeID;
    
    private String method;
    
    private String fromAmount;
    
    private String toAmount;
    
    private String commAmount;
    
    private String commPercentage;
    
    private String commtarget;
    
    private String gstn;
    
    private String pan;
    
    private String commSource;
    
    private List<Commission> commissionList;
    
    public Commission() {
    }

    public Commission(Long id) {
        this.id = id;
    }
    public Commission(Commission c) {
    	super();
        this.id = c.id;
        this.transTypeID = c.transTypeID;
        this.channelID = c.channelID;
        this.circleID = c.circleID;
        this.groupID = c.groupID;
        this.transAccountTypeID = c.transAccountTypeID;
        this.targetAccountTypeID = c.targetAccountTypeID;
        this.method = c.method;
        this.fromAmount = c.fromAmount;
        this.toAmount = c.toAmount;
        this.commAmount = c.commAmount;
        this.commtarget = c.commtarget;
        this.commSource = c.commSource;
        this.pan = c.pan;
        this.gstn = c.gstn;
    }
    public Commission(Long id, long transTypeID, long channelID, long circleID, long groupID, long transAccountTypeID, long targetAccountTypeID, String method, String fromAmount, String toAmount, String commAmount, String commtarget, String commSource,String pan,String gstn) {
        this.id = id;
        this.transTypeID = transTypeID;
        this.channelID = channelID;
        this.circleID = circleID;
        this.groupID = groupID;
        this.transAccountTypeID = transAccountTypeID;
        this.targetAccountTypeID = targetAccountTypeID;
        this.method = method;
        this.fromAmount = fromAmount;
        this.toAmount = toAmount;
        this.commAmount = commAmount;
        this.commtarget = commtarget;
        this.commSource = commSource;
        this.pan = pan;
        this.gstn = gstn;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public long getTransTypeID() {
        return transTypeID;
    }

    public void setTransTypeID(long transTypeID) {
        this.transTypeID = transTypeID;
    }

    public long getChannelID() {
        return channelID;
    }

    public void setChannelID(long channelID) {
        this.channelID = channelID;
    }

    public long getCircleID() {
        return circleID;
    }

    public void setCircleID(long circleID) {
        this.circleID = circleID;
    }

    public long getGroupID() {
        return groupID;
    }

    public void setGroupID(long groupID) {
        this.groupID = groupID;
    }

    public long getTransAccountTypeID() {
        return transAccountTypeID;
    }

    public void setTransAccountTypeID(long transAccountTypeID) {
        this.transAccountTypeID = transAccountTypeID;
    }

    public long getTargetAccountTypeID() {
        return targetAccountTypeID;
    }

    public void setTargetAccountTypeID(long targetAccountTypeID) {
        this.targetAccountTypeID = targetAccountTypeID;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getFromAmount() {
        return fromAmount;
    }

    public void setFromAmount(String fromAmount) {
        this.fromAmount = fromAmount;
    }

    public String getToAmount() {
        return toAmount;
    }

    public void setToAmount(String toAmount) {
        this.toAmount = toAmount;
    }

    public String getCommAmount() {
        return commAmount;
    }

    public void setCommAmount(String commAmount) {
        this.commAmount = commAmount;
    }

    public String getCommPercentage() {
        return commPercentage;
    }

    public void setCommPercentage(String commPercentage) {
        this.commPercentage = commPercentage;
    }

    public String getCommtarget() {
        return commtarget;
    }

    public void setCommtarget(String commtarget) {
        this.commtarget = commtarget;
    }

    public String getCommSource() {
        return commSource;
    }

    public void setCommSource(String commSource) {
        this.commSource = commSource;
    }

    public int getProductID() {
		return productID;
	}

	public void setProductID(int productID) {
		this.productID = productID;
	}

	public List<Commission> getCommissionList() {
		return commissionList;
	}

	public void setCommissionList(List<Commission> commissionList) {
		this.commissionList = commissionList;
	}

	public String getGstn() {
		return gstn;
	}

	public void setGstn(String gstn) {
		this.gstn = gstn;
	}

	public String getPan() {
		return pan;
	}

	public void setPan(String pan) {
		this.pan = pan;
	}

	@Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Commission)) {
            return false;
        }
        Commission other = (Commission) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "in.co.triotech.model.Commission[ id=" + id + " ]";
    }
    
}

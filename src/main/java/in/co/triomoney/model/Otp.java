/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package in.co.triomoney.model;

import java.io.Serializable;
import java.util.Date;

public class Otp implements Serializable {
    private static final long serialVersionUID = 1L;
    
    private Long id;
   
    private int otp;
    
    private Long transID;
    
    private Date created;
    
    private Date expiry_date;
    
    private Date lastModified;
   
    

    public Otp() {
    }

    public Otp(Long id) {
        this.id = id;
    }
   
    public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getOtp() {
		return otp;
	}

	public void setOtp(int otp) {
		this.otp = otp;
	}

	public Long getTransID() {
		return transID;
	}

	public void setTransID(Long transID) {
		this.transID = transID;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getExpiry_date() {
		return expiry_date;
	}

	public void setExpiry_date(Date expiry_date) {
		this.expiry_date = expiry_date;
	}

	public Date getLastModified() {
		return lastModified;
	}

	public void setLastModified(Date lastModified) {
		this.lastModified = lastModified;
	}

	public Otp(Long id, int otp, long transID,Date created,Date expiry_date,Date lastModified) {
        this.id = id;
        this.otp=otp;
        this.transID=transID;
        this.created=created;
        this.expiry_date=expiry_date;
        this.lastModified=lastModified;
    }

	
   
    
}

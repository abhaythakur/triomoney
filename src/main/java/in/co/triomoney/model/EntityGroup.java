/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package in.co.triomoney.model;

import java.io.Serializable;


public class EntityGroup implements Serializable {
    private static final long serialVersionUID = 1L;
    
    private Long id;
    
    private String name;
    
    private String description;
    
    private int ownerId;
    
    private String type;

    public EntityGroup() {
    }

    public EntityGroup(Long id) {
        this.id = id;
    }

    public EntityGroup(Long id, String name) {
        this.id = id;
        this.name = name;
    }
    public EntityGroup(Long id, String name, String description , int ownerID, String type) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.ownerId = ownerID;
        this.type = type;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getOwnerId() {
		return ownerId;
	}

	public void setOwnerId(int ownerId) {
		this.ownerId = ownerId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EntityGroup)) {
            return false;
        }
        EntityGroup other = (EntityGroup) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "in.co.triotech.model.EntityGroup[ id=" + id + " ]";
    }
    
}

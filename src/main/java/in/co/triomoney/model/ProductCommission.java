package in.co.triomoney.model;

public class ProductCommission {
	private static final long serialVersionUID = 1L;
	private int id;                         
	private int transTypeID;                
	private int productID;                  
	private String channelCommissionPercentage;
	private String channelCommissionAmount;    
	private String created;                   
	private long createdBy;                 
	private boolean isActive;                  
	private String updated;                    
	private long updatedBy;                 
	private String fromAmount;                 
	private String toAmount;
	
	public ProductCommission()
	{
		
	}
	
	public ProductCommission(ProductCommission pc)
	{
		super();
		this.id=pc.id;
		this.transTypeID = pc.transTypeID;
		this.productID=pc.productID;
		this.channelCommissionAmount =pc.channelCommissionAmount;
		this.channelCommissionPercentage = pc.channelCommissionPercentage;
		this.created=pc.created;
		this.createdBy = pc.createdBy;
		this.isActive = pc.isActive;
		this.updated = pc.updated;
		this.updatedBy =pc.updatedBy;
		this.fromAmount = pc.fromAmount;
		this.toAmount =pc.toAmount;
	}
	
	public ProductCommission(int id,int transTypeID,int productID, String channelCommissionPercentage,String channelCommissionAmount,String created,long createdBy, boolean isActive, String updated,long updatedBy,String fromAmount,String toAmount)
	{
		super();
		this.id=id;
		this.transTypeID = transTypeID;
		this.productID=productID;
		this.channelCommissionAmount =channelCommissionAmount;
		this.channelCommissionPercentage = channelCommissionPercentage;
		this.created=created;
		this.createdBy = createdBy;
		this.isActive = isActive;
		this.updated = updated;
		this.updatedBy =updatedBy;
		this.fromAmount = fromAmount;
		this.toAmount =toAmount;
	}
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getTransTypeID() {
		return transTypeID;
	}
	public void setTransTypeID(int transTypeID) {
		this.transTypeID = transTypeID;
	}
	public int getProductID() {
		return productID;
	}
	public void setProductID(int productID) {
		this.productID = productID;
	}
	public String getChannelCommissionPercentage() {
		return channelCommissionPercentage;
	}
	public void setChannelCommissionPercentage(String channelCommissionPercentage) {
		this.channelCommissionPercentage = channelCommissionPercentage;
	}
	public String getChannelCommissionAmount() {
		return channelCommissionAmount;
	}
	public void setChannelCommissionAmount(String channelCommissionAmount) {
		this.channelCommissionAmount = channelCommissionAmount;
	}
	public String getCreated() {
		return created;
	}
	public void setCreated(String created) {
		this.created = created;
	}
	public long getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(long createdBy) {
		this.createdBy = createdBy;
	}
	public boolean isActive() {
		return isActive;
	}
	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}
	public String getUpdated() {
		return updated;
	}
	public void setUpdated(String updated) {
		this.updated = updated;
	}
	public long getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(long updatedBy) {
		this.updatedBy = updatedBy;
	}
	public String getFromAmount() {
		return fromAmount;
	}
	public void setFromAmount(String fromAmount) {
		this.fromAmount = fromAmount;
	}
	public String getToAmount() {
		return toAmount;
	}
	public void setToAmount(String toAmount) {
		this.toAmount = toAmount;
	}      
	
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package in.co.triomoney.model;

import java.io.Serializable;


public class TransactionTypes implements Serializable {
    private static final long serialVersionUID = 1L;
   
    private Long id;
    
    private String name;
    
    private Boolean isFinancial;
   
    private String description;
    private int spID;

    public TransactionTypes() {
    }

    public TransactionTypes(Long id) {
        this.id = id;
    }

    public TransactionTypes(Long id, String name, Boolean isFinancial) {
        this.id = id;
        this.name = name;
        this.isFinancial = isFinancial;
    }

    public TransactionTypes(Long id, String name, Boolean isFinancial, String description, int spID) {
		super();
		this.id = id;
		this.name = name;
		this.isFinancial = isFinancial;
		this.description = description;
		this.spID = spID;
	}

	public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getIsFinancial() {
        return isFinancial;
    }

    public void setIsFinancial(Boolean isFinancial) {
        this.isFinancial = isFinancial;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getSpID() {
		return spID;
	}

	public void setSpID(int spID) {
		this.spID = spID;
	}

	@Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TransactionTypes)) {
            return false;
        }
        TransactionTypes other = (TransactionTypes) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "in.co.triotech.model.TransactionTypes[ id=" + id + " ]";
    }
    
}

package in.co.triomoney;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;

@SpringBootApplication(exclude = { SecurityAutoConfiguration.class })
public class TriomoneyApplication {

	public static void main(String[] args) {
		SpringApplication.run(TriomoneyApplication.class, args);
	}
}

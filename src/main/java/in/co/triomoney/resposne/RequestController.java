package in.co.triomoney.resposne;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RequestController {

	@GetMapping("badrequest")
	public ResponseEntity<String> accessDenied() {
		return new ResponseEntity<>("Access denied", HttpStatus.SERVICE_UNAVAILABLE);

	}

}

/*package in.co.triomoney.interceptor;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.AsyncHandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.UrlPathHelper;

import in.co.triomoney.crypto.EncryptionDecryptionUtil;
import in.co.triomoney.crypto.MCrypt;
import in.co.triomoney.model.Accounts;
import in.co.triomoney.model.Entity;
import in.co.triomoney.model.SystemConfig;
import in.co.triomoney.model.Transaction;
import in.co.triomoney.service.AccountService;
import in.co.triomoney.service.EntityService;
import in.co.triomoney.service.TransactionService;
@Component
public class TransactionInterceptor implements AsyncHandlerInterceptor {
	private final TransactionService tService;
	private final EntityService eService;
	private final AccountService aService;
    
	
	 * public boolean preHandle(HttpServletRequest request, HttpServletResponse
	 * response, Object handler) throws Exception {
	 * 
	 * System.out.println("\n-------- TransactionInterceptor.preHandle --- ");
	 * System.out.println("Request URL: " + request.getRequestURL()); HandlerMethod
	 * handlerMethod = (HandlerMethod) handler; if
	 * (handlerMethod.getMethod().equals("post")) { String command =
	 * request.getParameter("command"); String tuid = request.getParameter("TUID");
	 * 
	 * String transType = null;
	 * System.out.println("Sorry! This URL is no longer used, Redirect to " +
	 * transType); if (StringUtils.isEmpty(command) ||
	 * StringUtils.containsWhitespace(command) || StringUtils.isEmpty(tuid) ||
	 * StringUtils.containsWhitespace(tuid)) { throw new
	 * Exception("Invalid User Id or Password. Please try again."); } else {
	 * response.sendRedirect(request.getContextPath() + "/" + transType); } } {
	 * response.sendRedirect(request.getContextPath() + "/badrequest"); } return
	 * true; }
	 
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		Boolean isSuccess = false;
		String channelId = "2";
		String merchantUniqueID = "0";
		String initiator = "";
		String command = "";
		String resourcePath = new UrlPathHelper().getPathWithinApplication(request);
		System.out.println("Request come again.");
		try {
			if (request.getParameterMap().containsKey("channelId")) {
				channelId = request.getParameter("channelId");
			}
			if (request.getParameterMap().containsKey("command")) {
				command = request.getParameter("command");
				command = command.replaceAll("\n,\r", "");

			}
			if (request.getParameterMap().containsKey("TUID")) {
				initiator = request.getParameter("TUID");

			}
			if (request.getParameterMap().containsKey("merchantUniqueID")) {
				merchantUniqueID = request.getParameter("TUID");

			}
			if (request.getMethod().equalsIgnoreCase("post")) {
				if (postToAuthenticate(request, resourcePath, command, initiator)) {
					vaidateActivity(command, initiator, merchantUniqueID, request, response);
					isSuccess = true;
				} else {

				}
			} else {
				responseObject(100, HttpServletResponse.SC_BAD_REQUEST, 0l, response, request);
			}
		} catch (InternalAuthenticationServiceException internalAuthenticationServiceException) {
			responseObject(100, HttpServletResponse.SC_BAD_REQUEST, 0l, response, request);

		} catch (AuthenticationException authenticationException) {
			responseObject(100, HttpServletResponse.SC_UNAUTHORIZED, 0l, response, request);

		}
		return isSuccess;
	}

	private boolean postToAuthenticate(HttpServletRequest httpRequest, String resourcePath, String command,
			String tuid) {
		if (!resourcePath.equals("PartnerAPI") && StringUtils.isEmpty(command)
				&& StringUtils.containsWhitespace(command) && StringUtils.isEmpty(tuid)
				|| StringUtils.containsWhitespace(tuid)) {
			return false;
		}
		return true;
	}

	public boolean vaidateActivity(String command, String initiator, String merchantUniqueID,
			HttpServletRequest request, HttpServletResponse response) {
		String commandType = null;
		String commandArray[] = null;
		Boolean checkEncrypt = false;
		Map<String, String> systemConfigs = null;
		String checkEncryptdata = command.substring((command.length() - 5), command.length());
		Transaction trans = new Transaction();
		trans.setInitiator(initiator);
		trans.setState("1");
		trans.setCreated(new Date());
		trans.setParam1(merchantUniqueID);
		try {
			if (checkEncryptdata.equals("00000") || checkEncryptdata.equals("22222")) {
				if (checkEncryptdata.equals("00000")) {
					trans.setChannelID(3);
				} else if (checkEncryptdata.equals("22222")) {
					trans.setChannelID(9);
				}
				try {
					command = EncryptionDecryptionUtil.doDecrypt(command.substring(0, command.length() - 5));
					commandArray = command.split("\\|");
					commandType = commandArray[0];
					checkEncrypt = true;
				} catch (Exception e) {
					trans.setState("3");
				}
			} else if (checkEncryptdata.equals("11111")) {
				trans.setChannelID(5);
				MCrypt crypter = new MCrypt();
				String textToDecrypt = command.substring(0, (command.length() - 5));
				try {
					command = new String(crypter.decrypt(textToDecrypt));
					commandArray = command.split("\\|");
					commandType = commandArray[0];
					checkEncrypt = true;
				} catch (Exception e) {
					trans.setState("3");
				}
			}
			try {
				List<SystemConfig> systemList = tService.getAllConfig();
				systemConfigs = systemList.stream()
						.collect(Collectors.toMap(SystemConfig::getSys_key, SystemConfig::getSys_val));
			} catch (Exception e) {
				responseObject(100, HttpServletResponse.SC_INTERNAL_SERVER_ERROR, 0l, response, request);
			}
			if (checkEncrypt) {
				try {
					Long transTypeId = tService.isTransTypeExists(commandType.toLowerCase());
					System.out.println("TransTypeID=" + transTypeId);
					if (transTypeId > 0) {
						trans.setTransTypeID(transTypeId);
						Transaction transaction = new Transaction();
						transaction = tService.save(trans);
						if (transaction.getId() > 0) {
							Entity entity = null;
							System.out.println("Transaction created=" + transaction.getId());
							try {
								entity = eService.getEntityByMsisdn(transaction.getInitiator());
							} catch (Exception e) {
								entity = null;
							}
							if (entity != null) {
								System.out.println("Entity found=" + entity.getId());
								if (entity.getStatusID() != 3) {
									String incorrectAttempts = systemConfigs.get("incorrectAttempts");
									if (!commandType.equalsIgnoreCase("forgotpwd")
											&& !commandType.equalsIgnoreCase("resendotp")
											&& eService.checkIncorrectAttempts(incorrectAttempts, entity.getId())) {
										responseObject(1000, HttpServletResponse.SC_OK, transaction.getId(), response,
												request);
									} else {
										System.out.println("IncorrectAttempts Correct.");
										if (eService.isMPincodeCorrect(commandArray[commandArray.length - 1].toString(),
												entity.getId()) || commandType.equals("loginWotp")
												|| commandType.equals("register") || commandType.equals("resendotp")
												|| commandType.equals("forgotpwd") || commandType.equals("addmoney")
												|| commandType.equals("addmoneyres")
												|| commandType.equals("addmoneyvalidtrans")) {
											System.out.println("Mpin Correct.");
											Accounts account = new Accounts();
											account = aService.getAccountsByEntity(entity.getId());
											if (account != null) {
												System.out.println("Accounts found.");
												request.setAttribute("entity", entity);
												request.setAttribute("account", account);
												request.setAttribute("transaction", transaction);
												request.setAttribute("commandArray", commandArray.toString());
												try {
													response.sendRedirect(request.getContextPath() + "/cb");
												} catch (Exception e) {
													// TODO Auto-generated catch block
													e.printStackTrace();
												}
											} else {
												System.out.println("Accounts not found.");
												responseObject(502, HttpServletResponse.SC_OK, transaction.getId(),
														response, request);
											}

										} else {
											responseObject(535, HttpServletResponse.SC_OK, transaction.getId(),
													response, request);
										}
									}
								} else {
									responseObject(999, HttpServletResponse.SC_OK, transaction.getId(), response,
											request);
								}
							} else {
								responseObject(501, HttpServletResponse.SC_OK, transaction.getId(), response, request);
							}
						} else {
							responseObject(500, HttpServletResponse.SC_INTERNAL_SERVER_ERROR, 0l, response, request);
						}
					} else {
						responseObject(506, HttpServletResponse.SC_BAD_REQUEST, 0l, response, request);
					}
				} catch (Exception e) {
					e.printStackTrace();
					// responseObject(500, HttpServletResponse.SC_INTERNAL_SERVER_ERROR, 0l,
					// response, request);
				}
			} else {
				responseObject(705, HttpServletResponse.SC_BAD_REQUEST, 0l, response, request);
			}

		} catch (Exception e) {
			trans.setState("3");
		}
		return true;

	}

	boolean responseObject(Integer resultCode, Integer status, Long tid, HttpServletResponse httpResponse,
			HttpServletRequest httpRequest) {
		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject.put("Status", "error");
			jsonObject.put("Result Code", resultCode);
			jsonObject.put("Result Namespace", "triomoney");
			jsonObject.put("TID", tid);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		httpResponse.setStatus(status);
		return setHttpRequestResponse(httpRequest, httpResponse, jsonObject);
	}

	boolean setHttpRequestResponse(HttpServletRequest httpRequest, HttpServletResponse httpResponse, JSONObject json) {
		httpResponse.addHeader("Content-Type", "application/json");
		try {
			httpResponse.getWriter().write(json.toString());
			httpResponse.getWriter().flush();
			httpResponse.getWriter().close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, //
			Object handler, ModelAndView modelAndView) throws Exception {
		System.out.println("Request URL::" + request.getRequestURL().toString()
				+ " Sent to Handler :: Current Time=" + System.currentTimeMillis());
	}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, //
			Object handler, Exception ex) throws Exception {

		// This code will never be run.
		System.out.println("\n-------- QueryStringInterceptor.afterCompletion --- ");
	}

	public TransactionInterceptor(TransactionService tService, EntityService eService, AccountService aService) {
		this.tService = tService;
		this.eService = eService;
		this.aService = aService;
	}

}*/

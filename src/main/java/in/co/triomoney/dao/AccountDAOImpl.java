package in.co.triomoney.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import in.co.triomoney.model.Accounts;

@Transactional
@Repository
public class AccountDAOImpl implements AccountDAO {
	private final JdbcTemplate jdbcTemplate;

	@Autowired
	public AccountDAOImpl(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	@Override
	public Accounts getAccountsByEntity(Long entityID) {
		String sql = "SELECT id,entityID,currentBalance,availableBalance,lowerLimit,upperLimit,typeID FROM Accounts WHERE entityID = ?";
		RowMapper<Accounts> rowMapper = new BeanPropertyRowMapper<Accounts>(Accounts.class);
		Accounts accounts = jdbcTemplate.queryForObject(sql, rowMapper, entityID);
		return accounts;
	}

}

package in.co.triomoney.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import in.co.triomoney.model.SystemConfig;
import in.co.triomoney.model.Transaction;

@Transactional
@Repository
public class TransactionDAOImpl implements TransactionDAO {
	private final JdbcTemplate jdbcTemplate;

	@Autowired
	public TransactionDAOImpl(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	@Override
	public Transaction save(Transaction transaction) {
		String query = "INSERT INTO Transaction(initiator,transTypeId,state,channelID,param1) VALUES (?,?,?,?,?)";
		KeyHolder holder = new GeneratedKeyHolder();
		jdbcTemplate.update(new PreparedStatementCreator() {
			@Override
			public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
				PreparedStatement ps = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
				ps.setString(1, transaction.getInitiator());
				ps.setLong(2, transaction.getTransTypeID());
				ps.setString(3, transaction.getState());
				ps.setLong(4, transaction.getChannelID());
				ps.setString(5, transaction.getParam1());
				return ps;
			}
		}, holder);
		transaction.setId(holder.getKey().longValue());
		return transaction;

	}
	/*
	 * public Transaction save(Transaction transaction) { String sql =
	 * "INSERT INTO Transaction(initiator,created,transTypeId,state,channelID,param1) VALUES (?,?,?,?,?,?)"
	 * ; long id = jdbcTemplate.update(sql, transaction.getInitiator(), new Date(),
	 * transaction.getTransTypeID(), transaction.getState(),
	 * transaction.getChannelID(), transaction.getParam1()); transaction.setId(id);
	 * return transaction; }
	 */

	@Override
	public boolean update(Transaction transaction) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean updateById(String query) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public long getLastTrans(Object reMSISDN, Object recMSISDN) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public long getRowID(String qry) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public boolean deleteRowByID(String qry) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Long isTransTypeExists(String transType) {
		String sql = "SELECT id FROM TransactionTypes WHERE name= ?";
		Long id = jdbcTemplate.queryForObject(sql, Long.class, transType);
		return id;
	}

	@Override
	public List<SystemConfig> getAllConfig() {
		String sql = "SELECT id, sys_key, sys_val FROM SystemConfig";
		RowMapper<SystemConfig> rowMapper = new SystemConfig();
		return this.jdbcTemplate.query(sql, rowMapper);
	}

}

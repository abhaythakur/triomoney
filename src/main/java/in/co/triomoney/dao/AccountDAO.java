package in.co.triomoney.dao;

import in.co.triomoney.model.Accounts;

public interface AccountDAO {
	public Accounts getAccountsByEntity(Long entityID);
}

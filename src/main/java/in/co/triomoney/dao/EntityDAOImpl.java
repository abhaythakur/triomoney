package in.co.triomoney.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import in.co.triomoney.model.Entity;

@Transactional
@Repository
public class EntityDAOImpl implements EntityDAO {
	private final JdbcTemplate jdbcTemplate;

	@Autowired
	public EntityDAOImpl(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	@Override
	public Entity save(Entity entity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean update(Entity entity) {
		// TODO Auto-generated method stub
		return false;
	}

	/*
	 * @Override public Entity getEntityByMsisdn(String msisdn) { // TODO
	 * Auto-generated method stub return null; }
	 */
	public Entity getEntityByMsisdn(String msisdn) {
		String sql = "SELECT id,msisdn,name,statusID,typeID,parentID,circleID,pin,param1,param2,param3,last_login FROM Entity WHERE msisdn = ?";
		RowMapper<Entity> rowMapper = new BeanPropertyRowMapper<Entity>(Entity.class);
		Entity entity = jdbcTemplate.queryForObject(sql, rowMapper, msisdn);
		return entity;
	}

	@Override
	public Entity getEntityById(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean checkIncorrectAttempts(String incorrectAttempts,Long entityID) {
		try {
			String sql = "SELECT count(*) FROM password WHERE  entityID=? and incorrectAttempts >=?";
			int count = jdbcTemplate.queryForObject(sql, int.class, entityID, incorrectAttempts);
			if (count == 0) {
				return false;
			} else {
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public boolean isMPincodeCorrect(String password,Long entityID) {
		try {
			String sql = "SELECT count(*) FROM password  WHERE entityID=? and password=sha1(?)";
			int count = jdbcTemplate.queryForObject(sql, int.class, entityID, password);
			if (count == 0) {
				return false;
			} else {
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
}

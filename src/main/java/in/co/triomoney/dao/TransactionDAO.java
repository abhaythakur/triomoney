package in.co.triomoney.dao;

import java.util.List;

import in.co.triomoney.model.SystemConfig;
import in.co.triomoney.model.Transaction;


public interface TransactionDAO {
	public Transaction save(Transaction transaction);

	public boolean update(Transaction transaction);

	public boolean updateById(String query);
	
	public Long isTransTypeExists(String transType);

	public long getLastTrans(Object reMSISDN, Object recMSISDN);

	public long getRowID(String qry);

	public boolean deleteRowByID(String qry);
	public List<SystemConfig> getAllConfig();
}

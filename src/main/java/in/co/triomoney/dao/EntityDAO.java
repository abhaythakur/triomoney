package in.co.triomoney.dao;

import in.co.triomoney.model.Entity;

public interface EntityDAO {
	public Entity save(Entity entity);

	public boolean update(Entity entity);

	public boolean checkIncorrectAttempts(String incorrectAttempts,Long entityID);

	public Entity getEntityByMsisdn(String msisdn);

	public Entity getEntityById(Long id);
	
	public boolean isMPincodeCorrect(String password,Long entityID);

}

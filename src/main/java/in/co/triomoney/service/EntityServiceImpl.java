package in.co.triomoney.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import in.co.triomoney.dao.EntityDAO;
import in.co.triomoney.model.Entity;

@Service
public class EntityServiceImpl implements EntityService {
	@Autowired
	private EntityDAO entityDAO;

	@Override
	public Entity save(Entity entity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean update(Entity entity) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Entity getEntityByMsisdn(String msisdn) {
		return entityDAO.getEntityByMsisdn(msisdn);
	}

	@Override
	public Entity getEntityById(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean checkIncorrectAttempts(String incorrectAttempts,Long entityID) {
		return entityDAO.checkIncorrectAttempts(incorrectAttempts, entityID);
	}

	@Override
	public boolean isMPincodeCorrect(String password,Long entityID) {
		return entityDAO.isMPincodeCorrect(password, entityID);
	}

}

package in.co.triomoney.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import in.co.triomoney.dao.AccountDAO;
import in.co.triomoney.model.Accounts;

@Service
public class AccountServiceImpl implements AccountService {
	@Autowired
	AccountDAO accountDAO;

	@Override
	public Accounts getAccountsByEntity(Long entityID) {
		return accountDAO.getAccountsByEntity(entityID);
	}

}

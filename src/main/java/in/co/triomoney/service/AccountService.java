package in.co.triomoney.service;

import in.co.triomoney.model.Accounts;

public interface AccountService {
	public Accounts getAccountsByEntity(Long entityID);
}

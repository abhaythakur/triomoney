package in.co.triomoney.service;

import java.util.List;

import in.co.triomoney.model.SystemConfig;
import in.co.triomoney.model.Transaction;

public interface TransactionService {
	public Long isTransTypeExists(String transType);
	public Transaction save(Transaction transaction);
	public List<SystemConfig> getAllConfig();


}

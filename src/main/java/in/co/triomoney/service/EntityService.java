package in.co.triomoney.service;

import in.co.triomoney.model.Entity;

public interface EntityService {
	public Entity save(Entity entity);

	public boolean update(Entity entity);

	public Entity getEntityByMsisdn(String msisdn);

	public Entity getEntityById(Long id);

	public boolean checkIncorrectAttempts(String incorrectAttempts, Long entityID);

	public boolean isMPincodeCorrect(String password,Long entityID);

}

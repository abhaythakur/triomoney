package in.co.triomoney.service;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import in.co.triomoney.model.Accounts;
import in.co.triomoney.model.Entity;
import in.co.triomoney.model.SystemConfig;
import in.co.triomoney.model.Transaction;

@Service
public class BasicTransactionCheck {
	@Autowired
	TransactionService tService;
	@Autowired
	EntityService eService;
	@Autowired
	AccountService aService;

	public BasicTransactionCheck() {

	}

	public Object[] vaidateActivity(String commandType, String command, String initiator, String merchantUniqueID,
			Long channelId) {
		String commandArray[] = command.split("\\|");
		Object[] returnObj = new Object[5];
		Map<String, String> systemConfigs = null;
		Transaction trans = null;
		Transaction transaction = null;
		Entity entity = null;
		Accounts account = null;
		trans = new Transaction();
		trans.setInitiator(initiator);
		trans.setChannelID(channelId);
		trans.setState("1");
		trans.setCreated(new Date());
		trans.setParam1(merchantUniqueID);
		try {
			Long transTypeId = tService.isTransTypeExists(commandType.toLowerCase());
			System.out.println("TransTypeID=" + transTypeId);
			if (transTypeId > 0) {
				trans.setTransTypeID(transTypeId);
				transaction = new Transaction();
				transaction = tService.save(trans);
				if (transaction.getId() > 0) {
					System.out.println("Transaction created=" + transaction.getId());
					try {
						entity = eService.getEntityByMsisdn(transaction.getInitiator());
					} catch (Exception e) {
						entity = null;
					}
					if (entity != null) {
						try {
							List<SystemConfig> systemList = tService.getAllConfig();
							systemConfigs = systemList.stream()
									.collect(Collectors.toMap(SystemConfig::getSys_key, SystemConfig::getSys_val));
						} catch (Exception e) {
						}
						if (entity.getStatusID() != 3) {
							String incorrectAttempts = systemConfigs.get("incorrectAttempts");
							if (!commandType.equalsIgnoreCase("forgotpwd") && !commandType.equalsIgnoreCase("resendotp")
									&& eService.checkIncorrectAttempts(incorrectAttempts, entity.getId())) {
								returnObj[0] = 1000;
							} else {
								if (eService.isMPincodeCorrect(commandArray[commandArray.length - 1].toString(),
										entity.getId()) || commandType.equals("loginWotp")
										|| commandType.equals("register") || commandType.equals("resendotp")
										|| commandType.equals("forgotpwd") || commandType.equals("addmoney")
										|| commandType.equals("addmoneyres")
										|| commandType.equals("addmoneyvalidtrans")) {
									account = new Accounts();
									account = aService.getAccountsByEntity(entity.getId());
									if (account != null) {
										System.out.println("Accounts found.");
										returnObj[0] = 0;
									} else {
										returnObj[0] = 502;
									}

								} else {
									returnObj[0] = 535;
								}
							}
						} else {
							returnObj[0] = 999;
						}
					} else {
						returnObj[0] = 501;
					}
				} else {
					returnObj[0] = 500;
				}
			} else {
				returnObj[0] = 506;
			}
		} catch (Exception e) {
			e.printStackTrace();
			// responseObject(500, HttpServletResponse.SC_INTERNAL_SERVER_ERROR, 0l,
			// response, request);
		}
		returnObj[1] = transaction;
		returnObj[2] = entity;
		returnObj[3] = account;
		returnObj[4] = systemConfigs;
		return returnObj;

	}
}

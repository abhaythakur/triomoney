package in.co.triomoney.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import in.co.triomoney.dao.TransactionDAO;
import in.co.triomoney.model.SystemConfig;
import in.co.triomoney.model.Transaction;

@Service
public class TransactionServiceImpl implements TransactionService {
	@Autowired
	private TransactionDAO transactionDAO;

	@Override
	public Long isTransTypeExists(String transType) {
		return transactionDAO.isTransTypeExists(transType);
	}

	@Override
	public Transaction save(Transaction transaction) {
		return transactionDAO.save(transaction);
	}

	@Override
	public List<SystemConfig> getAllConfig() {
		return transactionDAO.getAllConfig();
	}

}

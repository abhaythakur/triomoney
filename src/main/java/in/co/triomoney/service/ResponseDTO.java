package in.co.triomoney.service;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Service;

@Service
public class ResponseDTO {

	public ResponseDTO() {

	}

	public JSONObject makeResponseByResultCode(Integer resultCode, Long tid) {
		JSONObject jsonObject = new JSONObject();
		try {
			if (resultCode == 0) {
				jsonObject.put("Status", "success");
			} else {
				jsonObject.put("Status", "error");
			}
			jsonObject.put("Result Code", resultCode);
			jsonObject.put("Result Namespace", "triomoney");
			jsonObject.put("TID", tid);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return jsonObject;
	}
}

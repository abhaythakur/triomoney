package in.co.triomoney.config;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import in.co.triomoney.crypto.EncryptionDecryptionUtil;
import in.co.triomoney.crypto.MCrypt;

@Component
public class RedirectingFilter extends OncePerRequestFilter {

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {
		if (!request.getRequestURI().endsWith("/")) {
			ServletUriComponentsBuilder builder = ServletUriComponentsBuilder.fromRequest(request);
			String commandType = null;
			String commandArray[] = null;
			Boolean checkEncrypt = false;
			String channelId = "2";
			String merchantUniqueID = "0";
			String initiator = "";
			String command = "";
			if (request.getParameterMap().containsKey("channelId")) {
				channelId = request.getParameter("channelId");
			}
			if (request.getParameterMap().containsKey("command")) {
				command = request.getParameter("command");
				command = command.replaceAll("\n,\r", "");

			}
			if (request.getParameterMap().containsKey("TUID")) {
				initiator = request.getParameter("TUID");

			}
			if (request.getParameterMap().containsKey("merchantUniqueID")) {
				merchantUniqueID = request.getParameter("TUID");

			}
			String checkEncryptdata = command.substring((command.length() - 5), command.length());
			if (checkEncryptdata.equals("00000") || checkEncryptdata.equals("22222")) {
				try {
					channelId = "3";
					command = EncryptionDecryptionUtil.doDecrypt(command.substring(0, command.length() - 5));
					commandArray = command.split("\\|");
					commandType = commandArray[0].toString();
					checkEncrypt = true;
				} catch (Exception e) {

				}
			} else if (checkEncryptdata.equals("11111")) {
				channelId = "5";
				MCrypt crypter = new MCrypt();
				String textToDecrypt = command.substring(0, (command.length() - 5));
				try {
					command = new String(crypter.decrypt(textToDecrypt));
					commandArray = command.split("\\|");
					commandType = commandArray[0].toString();
					checkEncrypt = true;
				} catch (Exception e) {
				}
			}
			if (checkEncrypt) {
				/*
				 * Constructor<?>[] publicConstructors = null; Object obj=null; Class<?>
				 * rectangleDefinition = null; try { rectangleDefinition =
				 * Class.forName("in.co.triomoney.model.CheckBalance"); } catch
				 * (ClassNotFoundException e) { e.printStackTrace(); } try { publicConstructors
				 * = Class.forName(rectangleDefinition.getCanonicalName()).getConstructors(); }
				 * catch (SecurityException | ClassNotFoundException e) {
				 * 
				 * } Object commParames[] = new Object[commandArray.length-1]; try { for (int i
				 * = 0; i < commParames.length; i++) commParames[i] = commandArray[i+1]; obj =
				 * createObject(publicConstructors, commParames); } catch (Exception e) {
				 * e.printStackTrace(); }
				 */
				builder.replacePath(String.format("%s/", builder.build().getPath() + "/" + commandType + "/" + command
						+ "/" + initiator + "/" + merchantUniqueID+"/"+channelId));
				response.setStatus(HttpStatus.MOVED_PERMANENTLY.value());
				response.setHeader(HttpHeaders.LOCATION, builder.toUriString());

			} else {
				((HttpServletResponse) response).setStatus(HttpServletResponse.SC_BAD_REQUEST);
			}
		} else {
			filterChain.doFilter(request, response);
		}
	}

	public RedirectingFilter() {

	}
	/*
	 * public Object createObject(Constructor<?>[] constructor, Object[] arguments)
	 * { Object object = null;
	 * 
	 * try { System.out.println(arguments.length); for (int i = 0; i <
	 * constructor.length; i++) {
	 * System.out.println(constructor[i].getParameterTypes().length); if
	 * (constructor[i].getParameterTypes().length == arguments.length) object =
	 * constructor[i].newInstance(arguments); } } catch (InstantiationException e) {
	 * System.out.println(e); } catch (IllegalAccessException e) {
	 * System.out.println(e); } catch (IllegalArgumentException e) {
	 * System.out.println(e); } catch (InvocationTargetException e) {
	 * System.out.println(e); } return object; }
	 */
}
package in.co.triomoney.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
public class AppConfig extends WebMvcConfigurerAdapter {
	private RedirectingFilter redirectingFilter() {
		return new RedirectingFilter();
	}
}

package in.co.triomoney.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import in.co.triomoney.interceptor.TransactionInterceptor;
import in.co.triomoney.service.AccountService;
import in.co.triomoney.service.EntityService;
import in.co.triomoney.service.TransactionService;
@SuppressWarnings("deprecation")
@Configuration
public class Config extends WebMvcConfigurerAdapter{
	@Autowired
	TransactionService tService;
	@Autowired
	EntityService eService;
	@Autowired
	AccountService aService;
	@Override
	   public void addInterceptors(InterceptorRegistry registry) {
	      // LogInterceptor apply to all URLs.
	      registry.addInterceptor(new TransactionInterceptor(tService,eService,aService));
	     
	   }
}

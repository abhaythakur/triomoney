package in.co.triomoney.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/PartnerAPI")
public class LoginController {

	@RequestMapping("/login")
	public String login() {
		return "login";
	}
}
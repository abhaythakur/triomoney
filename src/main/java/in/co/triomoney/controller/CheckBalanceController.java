package in.co.triomoney.controller;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import in.co.triomoney.model.Accounts;
import in.co.triomoney.model.Transaction;
import in.co.triomoney.service.BasicTransactionCheck;
import in.co.triomoney.service.ResponseDTO;

@RestController
@RequestMapping("/PartnerAPI")
public class CheckBalanceController {
	@Autowired
	BasicTransactionCheck transactionCheck;
	@Autowired
	ResponseDTO responseDTO;

	/*
	 * obj[1] = transaction; obj[2] = entity; obj[3] = account; obj[4] =
	 * systemConfigs;
	 */
	@RequestMapping("/cb/{command}/{initiator}/{merchantUniqueID}/{channelId}")
	public String getBalance(@PathVariable("command") String command, @PathVariable("initiator") String initiator,
			@PathVariable("merchantUniqueID") String merchantUniqueID, @PathVariable("channelId") Long channelId) {
		JSONObject jsonObj = new JSONObject();
		try {
			Object[] obj = transactionCheck.vaidateActivity("cb", command, initiator, merchantUniqueID, channelId);
			Transaction transaction = (Transaction) obj[1];
			Integer resultCode = (Integer) obj[0];
			if (resultCode == 0) {
				Accounts accounts = (Accounts) obj[3];
				jsonObj = responseDTO.makeResponseByResultCode(resultCode, transaction.getId());
				jsonObj.accumulate("balance", String.format("%.2f", accounts.getAvailableBalance()));
			} else {
				jsonObj = new ResponseDTO().makeResponseByResultCode(resultCode, transaction.getId());
			}

		} catch (Exception e) {

		}
		return jsonObj.toString();
	}
}
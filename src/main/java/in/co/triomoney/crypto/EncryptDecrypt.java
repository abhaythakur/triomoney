package in.co.triomoney.crypto;

import java.security.spec.KeySpec;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

public class EncryptDecrypt {
	private final static String ALGORITHM = "AES";
    private final static String HEX = "0123456789ABCDEF"; 


public static String cipher(String secretKey, String data) throws Exception {

        SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
        KeySpec spec = new PBEKeySpec(secretKey.toCharArray());
        SecretKey tmp = factory.generateSecret(spec);
        SecretKey key = new SecretKeySpec(tmp.getEncoded(), ALGORITHM);

        Cipher cipher = Cipher.getInstance(ALGORITHM);
        cipher.init(Cipher.ENCRYPT_MODE, key);

        return toHex(cipher.doFinal(data.getBytes()));
    }

    public static String decipher(String secretKey, String data) throws Exception {        
        SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
        KeySpec spec = new PBEKeySpec(secretKey.toCharArray());
        SecretKey tmp = factory.generateSecret(spec);
        SecretKey key = new SecretKeySpec(tmp.getEncoded(), ALGORITHM);

        Cipher cipher = Cipher.getInstance(ALGORITHM);

        cipher.init(Cipher.DECRYPT_MODE, key);

        return new String(cipher.doFinal(toByte(data)));
    }

// Helper methods

    private static byte[] toByte(String hexString) {
        int len = hexString.length()/2;

        byte[] result = new byte[len];

        for (int i = 0; i < len; i++)
            result[i] = Integer.valueOf(hexString.substring(2*i, 2*i+2), 16).byteValue();
        return result;
    }

    public static String toHex(byte[] stringBytes) {
        StringBuffer result = new StringBuffer(2*stringBytes.length);

        for (int i = 0; i < stringBytes.length; i++) {
            result.append(HEX.charAt((stringBytes[i]>>4)&0x0f)).append(HEX.charAt(stringBytes[i]&0x0f));
        }

        return result.toString();
    }
    public static void main(String[] args) throws Exception {
		//String Key = "super-secret-key-0123123451";
//		String  data="5D93CBB84E1B8F8622A25DB73235E7C66886BF9AD10FEE8BE560E2B284AC7C2D";
//		System.out.println(decipher(secretKey,data ));
		
		String text = "Rahul|9650526856|00000";
		String key = "super-secret-key-0123123451";
		String crypted="",decrypted="";
		try{
			crypted = cipher(key, text);
		   decrypted =decipher(key, crypted);
		}catch (Exception e){e.printStackTrace();}
		
		
		System.out.println("@@@@@@@@@@@@@@@@@@***************************************************");
		System.out.println("CRYPTO-TEST"+ "plain: " + text);
		System.out.println("CRYPTO-TEST"+ "crypted: " + crypted);
		System.out.println("CRYPTO-TEST"+"decrypted: " + decrypted);
		System.out.println("@@@@@@@@@@@@@@@@@@"+ "***************************************************");
		
	}
}

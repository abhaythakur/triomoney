package in.co.triomoney.crypto;
import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.crypto.engines.AESEngine;
import org.bouncycastle.crypto.modes.CBCBlockCipher;
import org.bouncycastle.crypto.paddings.PaddedBufferedBlockCipher;
import org.bouncycastle.crypto.params.KeyParameter;
import org.bouncycastle.crypto.params.ParametersWithIV;

/**
 *
 * @author Triotech
 */
public class EncryptionUtil {
	final static String PASSKEY = "test1234test1234";
	private static final String USER_AGENT = "Mozilla/5.0";

	public static void main(String[] args) throws Exception {
		// encrypt check balance code
		String ekey = "cb|1234";
		String response = doEncrypt(ekey);
		System.out.println(response+"00000" + "\n Length==>" + response.length());
	}

	private static byte[] cipherData(PaddedBufferedBlockCipher cipher, byte[] data) throws Exception {
		int minSize = cipher.getOutputSize(data.length);
		byte[] outBuf = new byte[minSize];
		int length1 = cipher.processBytes(data, 0, data.length, outBuf, 0);
		int length2 = cipher.doFinal(outBuf, length1);
		int actualLength = length1 + length2;
		byte[] result = new byte[actualLength];
		System.arraycopy(outBuf, 0, result, 0, result.length);
		return result;
	}

	private static byte[] encrypt(byte[] plain, byte[] key, byte[] iv) throws Exception {
		PaddedBufferedBlockCipher aes = new PaddedBufferedBlockCipher(new CBCBlockCipher(new AESEngine()));
		CipherParameters ivAndKey = new ParametersWithIV(new KeyParameter(key), iv);
		aes.init(true, ivAndKey);
		return cipherData(aes, plain);
	}

	public static String doEncrypt(String msg) throws Exception {
		byte[] enc = encrypt(msg.getBytes(), PASSKEY.getBytes(), PASSKEY.getBytes());
		String encBase = Base64.encode(enc);
		String encrypted = new String(encBase.getBytes(), "UTF-8");
		return encrypted;
	}

	private static final byte[] map = new byte[] { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N',
			'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i',
			'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '0', '1', '2', '3',
			'4', '5', '6', '7', '8', '9', '+', '/' };

	public static String encode(byte[] in) {
		int length = in.length * 4 / 3;
		length += length / 76 + 3; // for crlr
		byte[] out = new byte[length];
		int index = 0, i, crlr = 0, end = in.length - in.length % 3;
		for (i = 0; i < end; i += 3) {
			out[index++] = map[(in[i] & 0xff) >> 2];
			out[index++] = map[((in[i] & 0x03) << 4) | ((in[i + 1] & 0xff) >> 4)];
			out[index++] = map[((in[i + 1] & 0x0f) << 2) | ((in[i + 2] & 0xff) >> 6)];
			out[index++] = map[(in[i + 2] & 0x3f)];
			if (((index - crlr) % 76 == 0) && (index != 0)) {
				out[index++] = '\n';
				crlr++;
				// out[index++] = '\r';
				// crlr++;
			}
		}
		switch (in.length % 3) {
		case 1:
			out[index++] = map[(in[end] & 0xff) >> 2];
			out[index++] = map[(in[end] & 0x03) << 4];
			out[index++] = '=';
			out[index++] = '=';
			break;
		case 2:
			out[index++] = map[(in[end] & 0xff) >> 2];
			out[index++] = map[((in[end] & 0x03) << 4) | ((in[end + 1] & 0xff) >> 4)];
			out[index++] = map[((in[end + 1] & 0x0f) << 2)];
			out[index++] = '=';
			break;
		}
		return new String(out, 0, index);
	}

}